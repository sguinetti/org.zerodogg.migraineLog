# Migraine Log privacy policy

*Last change: 2021-02-05*

Migraine Log collects data that you provide to it for storage locally on your
device. This data never leaves your device unless you explicitly use the export
function to do so, and even then none of your data is ever transmitted to the
authors of Migraine Log, or any other third party unless you expressly do that
yourself with the exported file.

Migraine Log does not collect any analytics other than the analytics that are
collected by third parties like the Google Play store by default. If you elect
to submit crash logs, there is a chance that those may contain some personal
information (automatically gathered by the OS). Crash logs are not submitted
without explicit consent.

This privacy policy is subject to change, but the following terms will always
be in effect: Migraine Log will NEVER send any of your personal log data to the
owner or any third party, without your informed and explicit consent.
